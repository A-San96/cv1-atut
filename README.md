Classe virtuelle Africa Tech Up Tours
1.	Objectif
Durant la classe virtuelle, nous allons voir les notions de travail collaboratif avec GIT, scrapping/crawling, création d’API RESTful avec Python, tests automatisés ainsi que revue de qualité de code.

2.	Projet à implémenter
Il s’agit de créer un répertoire de recettes de cuisines africaines disponibles sur internet. On doit pouvoir fournir à tous les utilisateurs une procédure simple pour la préparation d’un repas.
Pour ce faire, on devra :
•	Déterminer plusieurs sources sur le net recensant les recettes de cuisines africaines
•	Collecter les informations sur les sites de références choisit
•	Enregistrer les données collectées dans une base de données
•	Exposer ces données grâce à une API
•	Permettre la création de nouvelles recettes sur le système

3.	Outils à utiliser
•	git : Git est un logiciel de gestion de versions décentralisé. C'est un logiciel libre créé par Linus Torvalds, auteur du noyau Linux, et distribué selon les termes de la licence publique générale GNU version 2. (Utilisateurs de Windows, installer git bash https://gitforwindows.org/. Utilisateurs de Linux, installer git grâce à la commande sudo apt-get install git)
•	beautifulsoup4 : Package python permettant de faire du scrapping. https://www.crummy.com/software/BeautifulSoup/bs4/doc/
•	requests : Package python permettant de faire de requête et de consommer une url. https://pypi.org/project/requests/
•	google : Permet d’effectuer des recherches google et de récupérer le lien de chaque résultat. https://github.com/MarioVilas/googlesearch 
•	pymongo : Package permettant la manipulation de base de données MongoDB. http://api.mongodb.com/python/current/tutorial.html
•	Flask : Framework python d'application Web WSGI léger. Il permet d’encapsuler un projet python en API RESTful. http://flask.pocoo.org/ 
•	flask_restplus : Flask-RESTPlus est une extension pour Flask qui prend en charge la création rapide d’API REST. Flask-RESTPlus encourage les meilleures pratiques avec une configuration minimale. Il fournit une collection cohérente de décorateurs et d'outils pour décrire votre API et exposer correctement sa documentation à l'aide de Swagger. https://medium.freecodecamp.org/structuring-a-flask-restplus-web-service-for-production-builds-c2ec676de563
•	SonarQube : Outil permettant d’exécuter, sur un projet, plusieurs types de tests de revue de qualité de code. https://www.sonarqube.org/ 
•	SonarScanner : SonarQube Scanner est recommandé comme lanceur par défaut pour analyser un projet avec SonarQube. https://www.sonarqube.org/

4.	Exercices à faire pour les classes virtuelles
Durant toute la période de formation, on verra la théorie relative à une notion donnée et vous devrez faire un exercice pratique afin de valider la session. Les différents exercices sont les suivants :
•	Création d’un compte sur www.gitlab.com
•	Tests de travail collaboratif : chaque participant devra ajouter ses informations (nom et prénom) à une liste et mettre à jour la liste sur gitlab
•	Création du dictionnaire de scrapping pour la collecte de données : chaque participant devra récupérer les informations de collecte (balise html et classe/id de la balise) sur la page web https://www.marmiton.org/recettes/recette_garba-cote-d-ivoire_35855.aspx pour chacune des informations de la recette (nom de la recette, nombre de personnes, durée de préparation, durée de cuisson, étapes de préparations, liste des ustensiles, liste des ingrédients)
•	Implémentation du scrapping en utilisant le dictionnaire de collecte définit
•	Création d’une simple API REST avec python
•	Création d’un serializer swagger pour une entité
•	Création d’une API REST avec swagger et connection à la base de données MongoDB
•	Tests de l’API avec SonarQube
