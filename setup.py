#Importation des librairies

from bs4 import BeautifulSoup
import requests
import re

#url = "https://www.marmiton.org/recettes/recette_tacos-mexicains_34389.aspx"
url = "https://www.marmiton.org/recettes/recette_garba-cote-d-ivoire_35855.aspx"

page = requests.get(url)
#verification de la disponibilité de la page
try:
    page = requests.get(url)
except:
    print("An error occured.")

soup=BeautifulSoup(page.text,"lxml")

nom_recette_html = soup.find("h1", class_="main-title")
nom_recette=nom_recette_html.string
print(nom_recette)


liste_des_ustensiles_html = soup.find_all("span", class_="recipe-utensil__name")

liste_des_ingredients_html = soup.find_all("li", class_="recipe-ingredients__list__item")


etapes_preparation_html = soup.find_all("li", class_="recipe-preparation__list__item")

duree_preparation_html = soup.find("div", class_="recipe-infos__timmings__preparation")


duree_cuisson_html = soup.find("div", class_="recipe-infos__timmings__cooking")

nbre_personne_html = soup.find("div", class_="recipe-infos__quantity")
print(nbre_personne_html)